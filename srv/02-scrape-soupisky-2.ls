require! {
  fs
  async
  request
}

years = [2003 to 2015]
years = for year in years
  m = fs.readFileSync "#__dirname/../data/1/#year.html"
    .toString!
    .match /frm-roster-filter-form-competition(.*?)<option value="([0-9]+)"( selected)?>Reprezentace/
  if m
    console.log year, m.2
  else
    console.log "NOTHING #year"
  {year, competition:m.2}
# console.log years
# return
<~ async.eachSeries years, ({year, competition}, cb) ->
  console.log year
  options =
    url: "http://www.hokej.cz/mladez/roster/31?roster-filter-season=#year&roster-filter-competition=#{competition}&roster-filter-team=140"
    gzip: yes
    encoding: null
  (err, response, body) <~ request options
  <~ fs.writeFile "#__dirname/../data/2/#year.html", body
  cb!
console.log "Done!"
