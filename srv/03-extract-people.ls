require! {
  jsdom:{env}
  jquery
  fs
  async
}

dir = "#__dirname/../data/2"
files = fs.readdirSync dir
# files.length = 1
persons = []
<~ async.eachSeries files, (file, cb) ->
  (err, data) <~ fs.readFile "#dir/#file"
  data .= toString!
  (err, window) <~ env data
  $ = jquery window
  $ ".table-soupiska tbody td:nth-child(2) a" .each (person) ->
    console.log @getAttribute \href
    id = @getAttribute \href .split "/" .pop!
    persons[id] = 1
  cb!
# console.log files
ids = for id of persons
  id

fs.writeFileSync "#__dirname/../data/ids.txt", ids.join "\n"
