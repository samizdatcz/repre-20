require! {
  fs
  jsdom:{env}
  jquery
  async
}

files = fs.readdirSync "#__dirname/../data/hraci/"
outFile = fs.createWriteStream "#__dirname/../data/out.tsv", {flags: 'a'}
i = 357
if i == 0
  outFile.write "name\tborn\tmatches_youth\tmatches_repre\tgoals_youth\tgoals_repre\n"
else
  files .= slice i
async.eachSeries files, (file, cb) ->
  process.stdout.write "#i\t#file"
  i++
  (err, data) <~ fs.readFile "#__dirname/../data/hraci/#file"
  data .= toString!
  (err, window) <~ env data
  $ = jquery window
  id = file
  name = $ "h1.page__heading" .text!
  born = $ ".row.p-t-5 div:first-child span" .text!
  matches_youth = 0
  matches_repre = 0
  goals_youth = 0
  goals_repre = 0
  $ '#table--2 tbody:last-child tr' .each ->
    $line = $ @
    isRepre = !($line.find "td.tbl-c-1" .text!.match /mládež/)

    goals = parseInt do
      $line.find ".tbl-c-4" .text!
      10
    matches = parseInt do
      $line.find ".tbl-c-3" .text!
      10
    if isRepre
      matches_repre += matches
      goals_repre += goals
    else
      matches_youth += matches
      goals_youth += goals
  <~ outFile.write ([name, born, matches_youth, matches_repre, goals_youth, goals_repre].join "\t") + "\n"
  console.log "Done!"
  cb!

