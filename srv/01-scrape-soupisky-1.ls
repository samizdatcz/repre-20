require! {
  fs
  async
  request
}

years = [2003 til 2015]
<~ async.eachSeries years, (year, cb) ->
  console.log year
  options =
    url: "http://www.hokej.cz/mladez/roster/31?roster-filter-season=#year&roster-filter-competition=2361&roster-filter-team=140"
    gzip: yes
    encoding: null
  (err, response, body) <~ request options
  <~ fs.writeFile "#__dirname/../data/1/#year.html", body
  cb!
console.log "Done!"
