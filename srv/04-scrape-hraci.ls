require! {
  fs
  request
  async
}

ids = fs.readFileSync "#__dirname/../data/ids.txt"
  .toString!
  .split "\n"

# ids.length = 1

<~ async.eachSeries ids, (id, cb) ->
  console.log id
  options =
    url: "http://www.hokej.cz/hrac/#id"
    gzip: yes
    encoding: null
  (err, response, body) <~ request options
  fs.writeFileSync "#__dirname/../data/hraci/#id.html", body
  cb!
console.log "Done"
